// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FPS_MyCharacter_generated_h
#error "MyCharacter.generated.h already included, missing '#pragma once' in MyCharacter.h"
#endif
#define FPS_MyCharacter_generated_h

#define FPS_Source_FPS_MyCharacter_h_11_RPC_WRAPPERS
#define FPS_Source_FPS_MyCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define FPS_Source_FPS_MyCharacter_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend FPS_API class UClass* Z_Construct_UClass_AMyCharacter(); \
	public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, FPS, NO_API) \
	DECLARE_SERIALIZER(AMyCharacter) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AMyCharacter*>(this); }


#define FPS_Source_FPS_MyCharacter_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend FPS_API class UClass* Z_Construct_UClass_AMyCharacter(); \
	public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, FPS, NO_API) \
	DECLARE_SERIALIZER(AMyCharacter) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AMyCharacter*>(this); }


#define FPS_Source_FPS_MyCharacter_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AMyCharacter(const AMyCharacter& InCopy); \
public:


#define FPS_Source_FPS_MyCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AMyCharacter(const AMyCharacter& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCharacter)


#define FPS_Source_FPS_MyCharacter_h_8_PROLOG
#define FPS_Source_FPS_MyCharacter_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_MyCharacter_h_11_RPC_WRAPPERS \
	FPS_Source_FPS_MyCharacter_h_11_INCLASS \
	FPS_Source_FPS_MyCharacter_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_POP


#define FPS_Source_FPS_MyCharacter_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_MyCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	FPS_Source_FPS_MyCharacter_h_11_INCLASS_NO_PURE_DECLS \
	FPS_Source_FPS_MyCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_POP


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FPS_Source_FPS_MyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
